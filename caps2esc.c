#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <linux/input.h>

// clang-format off
static const struct input_event
esc_up          = {.type = EV_KEY, .code = KEY_ESC,      .value = 0},
ctrl_up         = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 0},
capslock_up     = {.type = EV_KEY, .code = KEY_CAPSLOCK, .value = 0},
esc_down        = {.type = EV_KEY, .code = KEY_ESC,      .value = 1},
ctrl_down       = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 1},
capslock_down   = {.type = EV_KEY, .code = KEY_CAPSLOCK, .value = 1},
esc_repeat      = {.type = EV_KEY, .code = KEY_ESC,      .value = 2},
ctrl_repeat     = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 2},
capslock_repeat = {.type = EV_KEY, .code = KEY_CAPSLOCK, .value = 2},
syn             = {.type = EV_SYN, .code = SYN_REPORT,   .value = 0};
// clang-format on

int equal(const struct input_event *first, const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
        exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    double sec = 0; //not really seconds but close enough, init to 0 to mimic original caps2esc
    if (argc > 2 || (argc == 2 && sscanf(argv[1], "%lf", &sec) != 1) || sec < 0) {
        printf("usage: %s [seconds]\n"
               "[seconds] - number of seconds after which pressed CapsLock "
               "starts to be automatically interpreted as Ctrl; "
               "expected to be a positive real number, if it's omited or 0, "
               "CapsLock will be interpreted as Ctrl only when used "
               "in combination with other keys\n", argv[0]);
        return EXIT_FAILURE;
    }
    const double LONG_PRESS = sec * 1000 / 35; //one EV_MSC event every ~35ms on my keyboard

    int capslock_is_down = 0, esc_possible = 1, count = 0;
    struct input_event input;

    setbuf(stdin, NULL); setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN) {
            //detect long press by counting EV_MSC MSC_SCAN events
            if (LONG_PRESS && capslock_is_down && esc_possible && ++count > LONG_PRESS) {
                esc_possible = 0;
                write_event(&ctrl_down);
                write_event(&syn);
                usleep(20000);
            }
            continue;
        }

        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }

        if (capslock_is_down) {
            if (equal(&input, &capslock_down) || equal(&input, &capslock_repeat))
                continue;

            if (equal(&input, &capslock_up)) {  //CapsLock release
                if (esc_possible) {                 //it's Esc
                    write_event(&esc_down);
                    write_event(&syn);
                    usleep(20000);
                    write_event(&esc_up);
                } else {                            //it's Ctrl
                    write_event(&ctrl_up);
                }
                //reset
                capslock_is_down = 0;
                esc_possible = 1;
                count = 0;
                continue;
            }

            if (esc_possible && input.value) { //key combination - it's Ctrl
                esc_possible = 0;
                write_event(&ctrl_down);
                write_event(&syn);
                usleep(20000);
            }
        } else if (equal(&input, &capslock_down)) {
            capslock_is_down = 1;
            continue;
        }

        write_event(&input);
    }
}
